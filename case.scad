use <bridge.scad>;

board_width=73;
board_height=53;
board_thickness=1;

hole_diameter=3;

module column() {
        cylinder(r=2.5, h=4, $fn=8, center=false);
        cylinder(r=1.3, h=14, $fn=8, center=false);
}

module board_support() {
        cylinder(r=2.5, h=4, $fn=8, center=false);
        cylinder(r=1.3, h=7, $fn=8, center=false);
}

module top_grid() {
    cube([25, 1.2, board_thickness], center=true);
    translate([0, -2.5, 0]) cube([25, 1.2, board_thickness], center=true);
    translate([0, -5, 0]) cube([25, 1.2, board_thickness], center=true);    
    translate([0, -7.5, 0])cube([25, 1.2, board_thickness], center=true);
    translate([0, -10, 0])cube([25, 1.2, board_thickness], center=true);
}

module side_grid() {
    cube([25, 1.2, board_thickness], center=true);
    translate([0, 0, -2.5]) cube([25, 1.2, board_thickness], center=true);
    translate([0, 0, -5]) cube([25, 1.2, board_thickness], center=true);    
    translate([0, 0, -7.5])cube([25, 1.2, board_thickness], center=true);
    translate([0, 0, -10])cube([25, 1.2, board_thickness], center=true);
}
module top() {
    difference() {
        difference() {
            cube([71+4, 51+4, board_thickness], center=true);
            translate([-27.5, 7, 0]) cube([22, 36, board_thickness], center=true);
        }
        translate([10, -8, 0]) top_grid();
    }
    //colulmns to press on pcb to lock in place
    translate([-22, -18, 0]) cylinder(r=2, h=23, $fn=8, center=false);
    translate([28, -14, 0]) cylinder(r=2, h=23, $fn=8, center=false);
    translate([10, 23, 0]) cylinder(r=2, h=23, $fn=8, center=false);
}
module wall1() {
    cube([71+4, board_thickness, 29], center=true);
}
module wall1_grid() {
    difference() {
        cube([71+4, board_thickness, 29], center=true);
        translate([10, 0, 5]) side_grid();
    }
}
module usb_hole() {
    cube([board_thickness, 10, 5], center=true);
}
module wall2() {
    difference() {
        cube([board_thickness, 51+4, 29], center=true);
        translate([0, 10, -6]) usb_hole();
    }
}

module matrix_insert_base() {
    cube([13, 37, board_thickness], center=true);
    translate([6, -15, 0]) cube([18, 7,board_thickness], center=true);
    translate([6, 15, 0]) cube([18, 7,board_thickness], center=true);   
}

module matrix_insert() {
    //right wall
    translate([10.5, -11, -5]) cube([22, board_thickness, 21], center=true);
    //left wall
    translate([10.5, 25, -5]) cube([22, board_thickness, 21], center=true);
    //back wall
    translate([21, 7, -5]) cube([board_thickness, 35, 21], center=true);
    //matrix base plate
    translate([6, 7, 5]) matrix_insert_base();
}
module wall3() {
    difference() {
        cube([board_thickness, 51+4, 29], center=true);
        translate([0, 7, -5]) 
          cube([1, 36, 20], center=true);
    }
    matrix_insert();
}



top();
translate([0, 27, 15]) wall1();
translate([0, -27, 15]) wall1_grid();
translate([37, 0, 15]) wall2();
translate([-37, 0, 15]) wall3();

translate([26, 24, 4]) {
    rotate([0, -90, 90]) {
        translate([8, 25, -5]) {
            cylinder(h=2, r=5, r=5);
        }
        translate([20, 10, -5]) {
            rotate([0, 180, 0]) {
                case_bridge();
        }
    }
    }
}
