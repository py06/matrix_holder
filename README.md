# Matrix Holder 

This repo contains the openscad files for the different 3d parts
deisgned to complete kalstock project. 

It is basically aimed at holding the led matrix and the arduino based PCB.



## Centerpice
centerpiece.scad is the part that will maintain the led matrix on top of some partition panel

## Board plate
board_plate.scad will containe the kalstock pcb. It will maintain the pcb through 3 columns

## Case
case.scad is the case on top of the board plate and which can also be placed on top of some partition board
