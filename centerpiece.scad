use<bridge.scad>;
use<matrix_holder.scad>;

case();
translate([8, 25, -5]) {
    cylinder(7, 5, 5);
}
translate([20, 10, -5]) {
    rotate([0, 180, 0]) {
        case_bridge();
    }
}
