board_width=71;
board_height=51;
board_thickness=1;

hole_diameter=3;

module corner_pane(l, h) {
    translate([l/2 -0.5 , 0, h/2]) cube([l, 1, h], center=true);
}
module corner(l, h) {
    corner_pane(l, h);
    rotate(a=90) corner_pane(l, h);
}

module column() {
        cylinder(r=2.5, h=4, $fn=8, center=false);
        cylinder(r=1.3, h=10, $fn=8, center=false);
}

module board_support() {
        cylinder(r=2.5, h=4, $fn=8, center=false);
        cylinder(r=1.3, h=7, $fn=8, center=false);
}

module base() {
        cube([board_width+4, board_height+4, board_thickness], center=true);
        //bottom left column
        translate([(-board_width/2)+6, (-board_height/2)+8,0]) {
            column();
        }
        //top left column
        translate([(-board_width/2)+6, (board_height/2)-6,0]) {
            column();
        }
        //top right column
        translate([(board_width/2)-6, (board_height/2)-6,0]) {
            column();
        }
        //bottom right column
        translate([(board_width/2)-18.7, (-board_height/2)+17.3,0]) {
            board_support();
        }
}

base();
translate([-(board_width/2 + 0.5), -(board_height/2+0.5), 0]) corner_pane(8, 8);
translate([(board_width/2 + 0.5), -(board_height/2+0.5), 0]) rotate(a=90)corner(8, 8);
translate([-(board_width/2 + 0.5), (board_height/2+0.5), 0]) rotate(a=-90)corner(8, 8);
translate([(board_width/2 + 0.5), (board_height/2+0.5), 0]) rotate(a=180)corner(8, 8);