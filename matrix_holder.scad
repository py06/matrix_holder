
matrix_width=33;
matrix_height=33;
matrix_depth=12;

holder_width=matrix_width*1.5;
thickness=5;

jumper_height=14;
jumper_width=5.5;
jumper_length=13;

module jumperhole()
{
   color("red") {
    translate([0, (holder_width / 2) - thickness/2 -1 , matrix_width /2 - (thickness/2)])
       {
            cube([jumper_height, jumper_width+2, jumper_height+2]);
        }
    }
}

module backhole()
{
    color("green") {
        translate([0, 4 , 8])
        {
            cube([thickness, 15, matrix_height - 6]);
        }
        translate([0, 31 , 8])
        {
            cube([thickness, 15, matrix_height - 6]);
        }
    }
}

module matrixcase()
{
    ergo=0.5;

    //base
    translate([0 , 0, 0]) {
        cube([matrix_depth + thickness  + ergo, holder_width, thickness]);
    }
    translate([matrix_depth + thickness, 0, thickness]) {
        cube([ergo, holder_width, ergo]);
    }

    //back
    translate([0, 0, thickness]) {
        difference() {
        cube([thickness, holder_width, matrix_height]);

        }
    }

    //top
    translate ([0, 0, matrix_height + thickness]) {
        cube([matrix_depth + thickness + ergo , holder_width, thickness]);
    }
    translate([matrix_depth + thickness, 0, matrix_height + thickness - ergo]) {
        cube([ergo, holder_width, ergo]);
    }


}

module case() {
	difference() {
	    difference(){
	        matrixcase();
	        jumperhole();
	    }
	    backhole();
	}
}

case();

