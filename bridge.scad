matrix_width=33;
matrix_height=33;
matrix_depth=12;

holder_width=matrix_width*1.5;
thickness=5;

jumper_height=14;
jumper_width=5.5;
jumper_length=13; 

module case_bridge()
{
    bridge_width=30;
    bridge_height=30;
    bridge_depth=17;
    bridge_thickness=3;
    
    cube([bridge_depth+(2*bridge_thickness), bridge_width, bridge_thickness]);
    difference() {
            difference() {
                cube([bridge_thickness, bridge_width, bridge_height]);
                translate([0,5,5]) {
                    cube([bridge_thickness, bridge_width/4, bridge_height-10]);
                }
            }
            translate([0,18,5]) {
                    cube([bridge_thickness, bridge_width/4, bridge_height-10]);
                }
        }
   
    translate([bridge_depth + bridge_thickness, 0, 0]) {
        //cube([bridge_thickness, bridge_width, bridge_height]);
        difference() {
            difference() {
                cube([bridge_thickness, bridge_width, bridge_height]);
                translate([0,5,5]) {
                    cube([bridge_thickness, bridge_width/4, bridge_height-10]);
                }
            }
            translate([0,18,5]) {
                    cube([bridge_thickness, bridge_width/4, bridge_height-10]);
                }
        }
   
   
    }
}

case_bridge();
